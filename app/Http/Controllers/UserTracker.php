<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserTracker extends Controller
{
    var $users;
    public function loadFromJson($json){
        
        $tracks=json_decode($json);
        $this->users=[];
        foreach($tracks as $track){
            $this->users[$track->user_id][]=['session_start'=>$track->session_start,'last_activity'=>$track->last_activity];
        }
    }

    public function listUsers(){
        return $this->users;
    }

    public function overallActivity(){

    }

    public function index(Request $req){
        /*
        $this->loadFromJson($req->json);
        $this->listUsers();
        */

        $json='[
            {
                "user_id":1,
                "session_start":"2019-06-03 08:55:19",
                "last_activity":"2019-06-03 09:05:19"
            },{
                "user_id":1,
                "session_start": "2019-06-03 07:05:19",
                "last_activity":"2019-06-03 07:25:19"
            },{
                "user_id":2,
                "session_start":"2019-06-03 06:05:19",
                "last_activity": "2019-06-03 09:06:19"
            },{
                "user_id":2,
                "session_start": "2019-06-03 09:05:19",
                "last_activity":"2019-06-03 09:15:19"
            }
        ]';

        $this->loadFromJson($json);
        //dd($this->listUsers());
        return view('usertracker',['users'=>$this->listUsers()]);
        return view('test');
    }
}
